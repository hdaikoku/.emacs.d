;; init.el
;; Author: Harunobu Daikoku <daikoku@hpcs.cs.tsukuba.ac.jp>
;; Require: Emacs 25.1+

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

;; include melpa archive in package-archives
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)

;; Avoid to write `package-selected-packages` in init.el
(setq custom-file "~/.emacs.d/inits/01-custom.el")
(load custom-file)

;; init-loader.el
(require 'init-loader)
(setq init-loader-show-log-after-init 'error-only)
(init-loader-load "~/.emacs.d/inits")
