;; move backup/auto-save files to ~/.emacs.d/backup
(add-to-list 'backup-directory-alist
                         (cons "." "~/.emacs.d/backup/"))
(setq auto-save-file-name-transforms
      `((".*" ,(expand-file-name "~/.emacs.d/backup/") t)))

;; yasnippets
(yas-global-mode 1)
(eval-after-load 'yasnippet
  '(add-to-list 'yas-snippet-dirs "~/.emacs.d/snippets/"))

;; flycheck
(add-hook 'after-init-hook #'global-flycheck-mode)
(setq flycheck-display-errors-delay 0.5)
